const mongoose = require('mongoose');
const userSchema = new mongoose.Schema ({

	firstName: {
		type: String
	},
	lastName: {
		type: String
	},
	mobileNo: {
		type: Number
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	productOrdered: [
	{
		totalAmount: {
			type: Number
	},
		purchasedOn: {
			type: Date,
			default: new Date()
	},
		productId: {
			type: String
	},
		userId: {
			type: String
		}
	}
	]

})

module.exports = mongoose.model("User", userSchema)
