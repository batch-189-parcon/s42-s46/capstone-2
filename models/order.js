const mongoose = require('mongoose');
const orderSchema = new mongoose.Schema({

	
	totalAmount: {
			type: Number,
	},
	purchasedOn: {
			type: Date,
			default: new Date()
	},
	orderForm: [
	{
		productId: {
			type: String,
			required: [true, "Product not found!"]
		},
		userId: {
			type: String,
			required: [true, "User not found!"]
		}
	}
	]
})

module.exports = mongoose.model("Order", orderSchema);