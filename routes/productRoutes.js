const express = require('express')
const router = express.Router()
const productController = require('../controllers/productController')
const auth = require("../auth")

//Add Product (Admin Only)
router.post("/addProduct", auth.verify, (request, response) => {

	const adminData = auth.decode(request.headers.authorization)

	if(adminData.isAdmin){
		productController.addProduct(request.body).then(productController => response.send(productController))
	} else {
		response.send("Admin function only!")
	}
})

//Get all active products
router.get("/",(request, response) => {
	productController.getAllActive(request.body).then(productController => response.send(productController))
})


//Get a single product
router.get("/:productId", (request, response) => {
	productController.getProduct(request.params).then(productController => response.send(productController))
})


//Update Product (Admin Only)
router.put("/:productId", auth.verify, (request, response) =>{

	const adminData = auth.decode(request.headers.authorization)

	if(adminData.isAdmin){
		productController.updateProduct(request.params, request.body).then(productController => response.send(productController))
	} else {

		response.send("Admin function only!")
	}
})


//Archive a product
router.put("/:productId/archive", auth.verify, (request, response) => {

	const data = {
		productId : request.params.productId,
		payload : auth.decode(request.headers.authorization).isAdmin
	}

	productController.productActive(data).then(resultFromController => response.send(resultFromController))
})




module.exports = router