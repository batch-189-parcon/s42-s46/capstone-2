const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const orderController = require('../controllers/orderController')
const auth = require("../auth")


router.post("/register", (request, response) =>{ userController.registerUser(request.body).then(resultFromController => response.send(resultFromController))

})


router.post("/checkEmail", (request, response)=> { 
	userController.checkEmailExists(request.body).then(resultFromController => response.send(resultFromController))
})



router.post("/login", (request, response) => {
	userController.loginUser(request.body).then(resultFromController => response.send(resultFromController))
})



router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	
	userController.getProfile({id : userData.id}).then(resultFromController => res.send(resultFromController));

});


router.put("/:userId/setAsAdmin", auth.verify, (request, response) => {

	const adminStats = auth.decode(request.headers.authorization)

	if(adminStats.isAdmin) {
		userController.changeUserAdmin(request.params).then(resultFromController => response.send(resultFromController))
	} else {
		response.send("Admin function only!")
	}
})

//Create Order
router.post("/checkout", auth.verify, (request, response) => {
	
	const adminStats = auth.decode(request.headers.authorization)

	let data = {
		userId: auth.decode(request.headers.authorization).id,
		isAdmin: auth.decode(request.headers.authorization),
		productId: request.body.productId,
	}

	if(adminStats.isAdmin){
		response.send("User function only")
	} else {
		orderController.createOrder(data).then(resultFromController => response.send(resultFromController))
	}
})


router.get("/orders", auth.verify, (request, response) => {

	// const adminStats = auth.decode(request.headers.authorization)

	if(adminStats.isAdmin){
		orderController.getOrder().then(resultFromController => response.send(resultFromController))
	} else {
		response.send("Admin function only!")
	}
})


router.get("/myOrders", auth.verify, (request, response) => {
	const userData = auth.decode(request.headers.authorization)

	if(userData.isAdmin){
		response.send('User function only!')
	} else {
		orderController.getMyOrder(request.body).then(resultFromController => response.send(resultFromController))
	}
})














module.exports = router;
