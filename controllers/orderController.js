const User = require('../models/user');
const Product = require('../models/product');
const Order = require('../models/order')
const auth = require('../auth');


module.exports.createOrder = (data) => {
	console.log(data)
	if(data.isAdmin == true){
		return false
	} else {
		console.log(data)
		let newOrder = new Order ({
			totalAmount: data.totalAmount,
			orderForm: ({
				productId: data.productId,
				userId: data.userId
			}),
			productOrdered: ({
				productId: data.productId,
				userId: data.userId
			})
		})
		return newOrder.save().then((order, error) =>{
			if (error) {
				console.log(newOrder)
				return false
			} else {
				return true
		}
	}
 )}
}



module.exports.getOrder = (requestBody) =>{
	return Order.find({}).then(result => {
		return result
	}) 
}



module.exports.getMyOrder = (requestBody) => {
	return Order.find({userId: requestBody.order}).then(result => {
		return result
	})
}

