const Product = require('../models/product')
const User = require('../models/user')
const Order = require('../models/order')

//Create product
module.exports.addProduct = (requestBody) => {
	let newProduct = new Product({
		name: requestBody.name,
		description: requestBody.description,
		price: requestBody.price,
		image: requestBody.image
	})

	return newProduct.save().then((product, error) => {
		if(error){

			return false

		} else {

			return true
		}
	})
}


//Get All Active Product
module.exports.getAllActive = (requestBody) =>{
	return Product.find({isActive: true}).then(result => {
		return result
	}) 
}


//Get a single product
module.exports.getProduct = (requestParams) =>{
	return Product.findById(requestParams.productId).then(result => {
		return result
	})
}


//Update Product
module.exports.updateProduct = (requestParams, requestBody) => {
	let updatedProduct = {
		name: requestBody.name,
		description: requestBody.description,
		price: requestBody.price,
		image: requestBody.image
	}

	return Product.findByIdAndUpdate(requestParams.productId, updatedProduct).then((course, error) =>{
		if(error){
			return false
		} else {
			return true
		}
	})
}



//Archive Product
module.exports.productActive = (data) => {

	return Product.findById(data.productId).then((result, error) => {

		if(data.payload === true) {

			result.isActive = false;

			return result.save().then((archivedProduct, error) => {

				if(error) {

					return false;
				} else {


					return true;
				}
			})

		} else {
			return false
		}

	})
}