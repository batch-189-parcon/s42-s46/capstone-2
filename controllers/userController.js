const User = require('../models/user');
const Product = require('../models/product');
const Order = require('../models/order')
const bcrypt = require('bcrypt');
const auth = require('../auth')



module.exports.checkEmailExists = (requestBody) => { 
	return User.find({email: requestBody.email}).then(result => {
		if (result.length > 0) {
			return true
		} else {
			return false
		}
	})
}

//USER REGISTER
module.exports.registerUser = (requestBody) => {
	let newUser = new User ({
		firstName: requestBody.firstName,
		lastName: requestBody.lastName,
		email: requestBody.email,
		mobileNo: requestBody.mobileNo,
		password: bcrypt.hashSync(requestBody.password, 10)

	})

	return newUser.save().then((user, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}



//USER & ADMIN LOGIN
module.exports.loginUser = (requestBody) => {
	return User.findOne({email: requestBody.email}).then(result => {
		if (result == null) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password)
			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}

module.exports.getProfile = (data) => {

	return User.findById(data.id).then(result => {

		//console.log(result)

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});

};


//Change User to Admin Status
module.exports.changeUserAdmin = (requestParams) => {
	let newStat ={
		isAdmin: true
	}

	return User.findByIdAndUpdate(requestParams.userId, newStat).then((stat, error) =>{
		if(error){
			return false
		} else {
			return true
		}
	})
}



// module.exports.changeUserAdmin = (requestParams, requestBody) => {
// 	let newStat ={
// 		isAdmin: requestBody.isAdmin
// 	}

// 	return User.findByIdAndUpdate(requestParams.userId, newStat).then((stat, error) =>{
// 		if(error){
// 			return "error here"
// 		} else {
// 			return true
// 		}
// 	})
// }
